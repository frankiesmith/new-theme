<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 */

get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div class="top-blocks">
		<div class="hero">
			<div class="hero-image">
			</div>
			<div class="hero-box">
				<h4>WELCOME TO
				</br>
				<span>YARMOUTH</span></h4>
				<p>Yarmouth embodies the spirit of Cape Cod – from the coastlines of Nantucket Sound and Cape Cod Bay, to the shops and businesses anchoring our community, the locals residing here for generations, and the extended families returning year after year.</p>
			</div>
		</div>
<div class="top-buttons">
	<div class="guide-button">
		<?php echo wp_get_attachment_image( get_post_meta( get_the_ID(), '_y2_cmb2_home_featured_featured_image_1_id', 1 ), 'medium' ); ?>
	</div>
	<div class="calendar-button">
		<?php echo wp_get_attachment_image( get_post_meta( get_the_ID(), '_y2_cmb2_home_featured_featured_image_2_id', 1 ), 'medium' ); ?>
	</div>
</div>
	</div>
	<div class="homepage-blocks-widgets">
		<div class="homepage-block-wrapper">
			<div class="blocks-title">
				<h1><span>Events</span> IN YARMOUTH</h1>
			</div>
			<ul class="homepage-featured-blocks">
				<?php
								$blocks = (array) get_post_meta( get_the_ID(), '_y2_cmb2_home_blocks', true );
								foreach ( $blocks as $i => $block ) {
									$class        	= '';
									$bg_image_url 	= wp_get_attachment_image_url( $block['image_id'], 'large' );

									if( "wide" === $block['image_size'] ){
										$class .= " wide ";
									}

									echo "<li class='${class}'>";
									echo "<a href='".esc_url( $block['url'] )."'>";
									echo '<span class="post-title">'.esc_html( $block['title'] ).'</span>';
									echo '<span class="img-bg" style="background-image:url('.$bg_image_url.');"></span>';
									echo "</a></li>";
								}
							?>
			</ul>
		</div>
	</div>

	<div class="content">

		<div class="inner-content grid-x grid-margin-x grid-padding-x">

			<main class="main small-12 large-12 medium-12 cell" role="main">
				<?php get_template_part( 'parts/loop', 'page' ); ?>

				<?php endwhile; endif; ?>
			</main>
			<!-- end #main -->

		</div>
		<!-- end #inner-content -->

	</div>
	<!-- end #content -->

	<?php get_sidebar(); ?>

	<?php get_footer(); ?>