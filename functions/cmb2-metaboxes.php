<?php

function y2_show_if_front_page( $cmb ) {
    // Don't show this metabox if it's not the front page template.
    if ( get_option( 'page_on_front' ) !== $cmb->object_id ) {
        return false;
    }
    return true;
}

add_action( 'cmb2_admin_init', 'y2_cmb2_homepage_metaboxes' );

function y2_cmb2_homepage_metaboxes(){
    $prefix = '_y2_cmb2_home_';

    $cmb_home_page_blocks = new_cmb2_box( array(
        'id'           => $prefix . 'block_box',
        'title'        => esc_html__( 'Homepage Blocks', 'yarmouth' ),
        'object_types' => array( 'page' ), // Post type
        'context'      => 'normal',
        'priority'     => 'high',
        'show_names'   => true, // Show field names on the left
        'show_on'      => 'y2_show_if_front_page', // Specific post IDs to display this metabox
    ) );
    $group_field_id = $cmb_home_page_blocks->add_field( array(
        'id'          => $prefix . 'blocks',
        'type'        => 'group',
        'options'     => array(
            'group_title'   => esc_html__( 'Block {#}', 'yarmouth' ), // {#} gets replaced by row number
            'add_button'    => esc_html__( 'Add Another Block', 'yarmouth' ),
            'remove_button' => esc_html__( 'Remove Block', 'yarmouth' ),
            'sortable'      => true,
//            'closed'        => true, // true to have the groups closed by default
        ),
    ) );
    $cmb_home_page_blocks->add_group_field( $group_field_id, array(
        'name'       => esc_html__( 'Block Title', 'yarmouth' ),
        'id'         => 'title',
        'type'       => 'text',
        // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    $cmb_home_page_blocks->add_group_field( $group_field_id, array(
        'name'       => esc_html__( 'Chambermaster Link', 'yarmouth' ),
        'id'         => 'url',
        'type'       => 'text_url',
        // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    $cmb_home_page_blocks->add_group_field( $group_field_id, array(
        'name'    => esc_html__( 'Image size', 'y2' ),
        'id'      => 'image_size',
        'type'    => 'radio_inline',
        'options' => array(
          'standard'  => __( 'Standard', 'y2' ),
          'wide'      => __( 'Wide', 'y2' ),
        ),
        'default' => 'standard',
    ) );
    $cmb_home_page_blocks->add_group_field( $group_field_id, array(
        'name' => esc_html__( 'Block Image', 'yarmouth' ),
        'id'   => 'image',
        'type' => 'file',
    ) );
}


add_action( 'cmb2_admin_init', 'y2_cmb2_homepage_featured_content');

function y2_cmb2_homepage_featured_content() {
	 $prefix = '_y2_cmb2_home_featured_';

    $top_blocks = new_cmb2_box( array(
        'id'           => $prefix . 'top_blocks',
        'title'        => esc_html__( 'Top Blocks', 'yarmouth' ),
        'object_types' => array( 'page' ), // Post type
        'context'      => 'normal',
        'priority'     => 'high',
        'show_names'   => true, // Show field names on the left
        'show_on'      => 'y2_show_if_front_page', // Specific post IDs to display this metabox
    ) );
	
	$top_blocks->add_field( array(
		'name'    => 'Featured Image 1',
		'desc'    => 'Upload an image.',
		'id'      => $prefix . 'featured_image_1',
		'type'    => 'file',
		// Optional:
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
		'text'    => array(
			'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
		),
		'preview_size' => 'small', // Image size to use when previewing in the admin.
	) );
	$top_blocks->add_field( array(
		'name'    => 'Featured Image 2',
		'desc'    => 'Upload an image.',
		'id'      => $prefix . 'featured_image_2',
		'type'    => 'file',
		// Optional:
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
		'text'    => array(
			'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
		),
		'preview_size' => 'small', // Image size to use when previewing in the admin.
	) );
}